# Welcome to Exploring Read the Docs#

Powered by [BindTuning](http://bindtuning.com)

## Table of contents

- [What's included](#what's-included)
- [Installation](#installation)
- [Documentation](#documentation)
- [Community](#community)
- [Versioning](#versioning)
- [Copyright and license](#copyright-and-license)


All web parts are provided to you in 2 different packaging options, both following Microsoft Add-In guidelines:
- **APP**, for tenants with a configured App Store
- **WSP**, no-code sandbox solution, recommended whenever sandbox is available

<a name="installation"></a>
## Installation

Please refer to the following KB articles for detailed instructions, depending on your installation scenario.

### 1st-time installation

- [Install an APP web part](https://support.bind.pt/hc/en-us/articles/212173586)
- [Install a WSP web part](https://support.bind.pt/hc/en-us/articles/212173366)

### Upgrade/update

- [Upgrade an APP web part](https://support.bind.pt/hc/en-us/articles/213764403)
- [Upgrade a WSP web part](https://support.bind.pt/hc/en-us/articles/212189186)

### Upgrading code-based sandboxed Web Parts to the Add-In model

Refer to this section, instead of the previous one, if you are **moving from v1.x.x to v2.x.x.x**

- [Upgrade to the Add-In model](https://support.bind.pt/hc/en-us/articles/212146826-Upgrading-code-based-sandboxed-Web-Parts-to-the-Add-In-model)

### Uninstall

- [Uninstall an APP web part](https://support.bind.pt/hc/en-us/articles/212193846)
- [Uninstall a WSP web part](https://support.bind.pt/hc/en-us/articles/212193906)

<a name="documentation"></a>
## Documentation

The following technical resources are available:

- Web Part User Guide (see download instructions below)
- [BindTuning Knowledge Base](https://support.bind.pt/hc/en-us)
- [BindTuning Video Channel](https://youtube.com/bindskins)

The Web Part documentation is available in your web parts download area. To download it proceed as follows:

- Login at [BindTuning web site](http://bindtuning.com)
- Go to your [Account Downloads](http://bindtuning.com/account/downloads)
- Select "Webparts"
- Click to "View webpart" in your web part
- On the right column, check the "Documentation" section
- Click to download the "User Guide"

<a name="community"></a>
## Community

Keep track of development and community news.

- See what others are discussing in our [Community Center](https://support.bind.pt/hc/en-us/community/posts)
- Read and subscribe to [BindTuning Academy Blog](http://academy.bindtuning.com)
- Follow [@bindskins on Twitter](https://twitter.com/bindskins)
- Follow [@bindskins on Facebook](https://facebook.com/bindskins)
- Follow [@BindTuning on Google+](https://plus.google.com/+Bindtuning)
- Follow [@BindTuning on LinkedIn](https://www.linkedin.com/company/bindtuning)
